from flask import Flask
from flask import render_template, jsonify, request, make_response, abort
import requests
import json

from celery.result import AsyncResult
from celery import Celery

from conf import REDIS_ADDRESS, REDIS_PORT

app = Flask(__name__, template_folder="./templates")

celery_app = Celery(
    backend=f'redis://{REDIS_ADDRESS}:{REDIS_PORT}',
    broker=f'redis://{REDIS_ADDRESS}:{REDIS_PORT}'
)
generate_prediction = celery_app.signature('worker_process.generate_prediction')

# Using the worker mock
# generate_prediction = celery_app.signature('worker_mock.generate_prediction')


@app.route('/', methods=['GET'])
def index():
    info = {}
    return render_template('index.html', **info)


@app.route('/prediction', methods=['POST'])
def request_prediction():
    text = request.json['message']
    result_task = generate_prediction.delay(text)
    print(text)

    response_dict = {'status': result_task.status,
                     'result_task_id': result_task.id}
    return make_response(jsonify(response_dict))

    # text = request.form['message']
    # # predicition_address = "http://prediction_api"
    # prediction_address = "http://processor:100"
    # # print(text)
    # # prediction_address = "http://localhost:8001"
    # header = {"Content-Type": "application/json"}
    # response = requests.post(prediction_address, json={"text": text}, headers=header)
    # info = response.content
    # # info = info.decode("utf8").replace("'", '"')
    # info = info.decode("utf8")
    # info = json.loads(info)
    # return make_response(jsonify(info))


@app.route('/show_result/<string:result_task_id>', methods=['GET'])
def show_result(result_task_id):
    result_task = AsyncResult(id=result_task_id, app=celery_app)
    response_dict = {
        'status': result_task.status,
        'content': result_task.info
    }
    try:
        return make_response(jsonify(response_dict))
    except TypeError:
        abort(500)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=90)

import time
from flask import Flask
from celery import Celery
from conf import REDIS_ADDRESS
from conf import REDIS_PORT

flask_app = Flask(__name__)
flask_app.config.update(
    CELERY_BROKER_URL=f'redis://{REDIS_ADDRESS}:{REDIS_PORT}',
    CELERY_RESULT_BACKEND=f'redis://{REDIS_ADDRESS}:{REDIS_PORT}'
)


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


celery_app = make_celery(flask_app)


@celery_app.task()
def generate_prediction(text):
    time.sleep(3)
    # dict_prediction = {'text': text, 'prediction': 'B1'}
    # dict_prediction = 'A1: 0%, A2: 0%, B1: 0.21%, B2: 31.99%, C1: 48.81%, C2: 18.94%'
    dict_prediction = 'B2: 31.99%, C1: 48.81%'
    # return {'first_class': '48.81% level B2',
    #         'second_class': '31.99% level C1'
    #         }
    return {'first_class': 'C2',
            'first_class_percentage': '48.81%',
            'second_class': 'C1',
            'second_class_percentage': '31.99%'
            }

    # return dict_prediction
